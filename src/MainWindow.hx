import avalonia.controls.TextBlock;
import avalonia.controls.Window;

@:nativeGen
class MainWindow extends Window {
	public function new() {
		super();
		initializeComponent();
	}

	function initializeComponent():Void {
		var txb = new TextBlock();
		txb.Text = "Test haxe app";
		this.Content = txb;
	}
}
