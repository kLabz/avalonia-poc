import cs.system.Uri;

import avalonia.Application;
import avalonia.controls.applicationlifetimes.IClassicDesktopStyleApplicationLifetime;
import avalonia.markup.xaml.styling.StyleInclude;

@:nativeGen
class App extends Application {
	@:overload
	override public function Initialize():Void {
		var style = new StyleInclude((null :Uri));
		style.Source = new Uri("resm:Avalonia.Themes.Default.DefaultTheme.xaml?assembly=Avalonia.Themes.Default");
		this.Styles.Add(style);

		style = new StyleInclude((null :Uri));
		style.Source = new Uri("resm:Avalonia.Themes.Default.Accents.BaseLight.xaml?assembly=Avalonia.Themes.Default");
		this.Styles.Add(style);
	}

	@:overload
	override public function OnFrameworkInitializationCompleted():Void {
		if (this.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime) {
			var desktop:IClassicDesktopStyleApplicationLifetime = cast this.ApplicationLifetime;
			desktop.MainWindow = new MainWindow();
		}

		super.OnFrameworkInitializationCompleted();
	}
}
