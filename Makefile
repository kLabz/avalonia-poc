build:
	cd .build && dotnet build

start:
	cd .build && dotnet run -r linux-x64

# ls -1 libs | sed -e 's/^/--net-lib .\/libs\//g' > net-libs.hxml
